package ar.fiuba.tdd.tp0;

import static org.junit.Assert.*;

import org.junit.Test;

public class CalculatorTest {

	protected static final double DELTA = 0.00001;
	protected final RPNCalculator calculator = new RPNCalculator();

	@Test
	public void sum() {
		calculator.addBynaryOperator(new PlusOperator());
		calculator.add(2);
		calculator.add(5);
		assertEquals(5 + 2, calculator.calculate(), DELTA);
	}
	
}
