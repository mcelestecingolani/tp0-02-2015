package ar.fiuba.tdd.tp0;

public class NOperator implements PolishAtom{
	
	private Operator op;
	
	public NOperator (Operator op){
		this.op = op;
	}

	@Override
	public void operate(MyStack src, MyStack result) {
		MyStack auxStack = new MyStack();
		while (!result.isEmpty()){
			auxStack.push(result.pop());
		}
		Number left = (Number) auxStack.pop();
		Number right;
		Number res = left;
		while (!auxStack.isEmpty()){
			right = (Number) auxStack.pop();
			res = op.execute(left, right);
			left = res;
		}
		result.push(res);
	}

}
