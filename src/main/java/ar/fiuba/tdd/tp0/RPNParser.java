package ar.fiuba.tdd.tp0;

import java.util.HashMap;
import java.util.Map;

public class RPNParser {
	
	private static final Map<String,PolishAtom > map = new HashMap<String,PolishAtom>();
	
	public RPNParser(){
		map.put("+", new BinaryOperator(new PlusOperator()));
		map.put("-", new BinaryOperator(new MinusOperator()));
		map.put("*", new BinaryOperator(new TimesOperator()));
		map.put("/", new BinaryOperator(new DivideOperator()));
		map.put("MOD", new BinaryOperator(new ModOperator()));
		map.put("++", new NOperator(new PlusOperator()));
		map.put("--", new NOperator(new MinusOperator()));
		map.put("**", new NOperator(new TimesOperator()));
		map.put("//", new NOperator(new DivideOperator()));
	}
	
	public void parse (String src, MyStack stack){
		String[] operands = src.split(" ");
		PolishAtom a;
		String operand;
		for (int i = operands.length-1 ; i >=0 ; i--){
			operand = operands[i];
			if (isNumber(operand)){
				a = new Number (operand);
			} else {
				a = map.get(operand);
			}
			stack.push(a);
		}
			
	}
	
	
	private boolean isNumber(String s ){
		return s.matches("^-?\\d+$");
	}
	


}
