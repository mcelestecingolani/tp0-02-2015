package ar.fiuba.tdd.tp0;

import java.util.LinkedList;

public class MyStack {
	
	private LinkedList <PolishAtom> values = new LinkedList<PolishAtom>();
	
	public PolishAtom pop (){
		return values.pop();
	}
	
	public void push (PolishAtom e){
		values.push(e);
	}
	
	public boolean isEmpty(){
		return values.isEmpty();
	}

}
