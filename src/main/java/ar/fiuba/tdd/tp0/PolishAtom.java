package ar.fiuba.tdd.tp0;

public interface PolishAtom {
	
	public abstract void operate (MyStack src, MyStack result);
	
}
