package ar.fiuba.tdd.tp0;


public interface Operator {
	
	public abstract Number execute (Number a, Number b);
}