package ar.fiuba.tdd.tp0;

public class MinusOperator implements Operator {

	@Override
	public Number execute(Number a, Number b) {
		return a.minus(b);
	}

}
