package ar.fiuba.tdd.tp0;

public class RPNCalculator {
	
	private MyStack src = new MyStack();

    public float eval(String expression) {
    	RPNParser p = new RPNParser();
    	p.parse(expression, src);
    	return calculate();
    }
    
    public void add (float f){
    	Number n = new Number (f);
    	src.push(n);
    }
    
    public void addBynaryOperator (Operator op){
    	BinaryOperator bop = new BinaryOperator (op);
    	src.push(bop);
    }
    
    public float calculate(){
    	MyStack result = new MyStack();
    	PolishAtom actual;
    	while (!src.isEmpty()){
    		actual = src.pop();
    		actual.operate(src,result);
    	}
    	actual = result.pop();
    	return ((Number)actual).getValue();
    }

}
