package ar.fiuba.tdd.tp0;

public class DivideOperator implements Operator {

	@Override
	public Number execute(Number a, Number b) {
		return a.divide(b);
	}

}
