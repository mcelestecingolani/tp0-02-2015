package ar.fiuba.tdd.tp0;

public class PlusOperator implements Operator {

	@Override
	public Number execute(Number a, Number b) {
		return a.plus(b);
	}

}
