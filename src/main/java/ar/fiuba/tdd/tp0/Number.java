package ar.fiuba.tdd.tp0;

public class Number implements PolishAtom{
	
	private float value;

	public Number (float value){
		this.value = value;
	}
	
	public Number (String s){
		this.value = new Float (s).floatValue();
	}

	public float getValue (){
		return value;
	}
	
	@Override
	public void operate(MyStack src, MyStack result) {
		result.push(this);
	}

	public Number plus(Number b) {
		return new Number (this.value + b.value);
	}

	public Number minus(Number b) {
		return new Number (this.value - b.value);
	}

	public Number times(Number b) {
		return new Number (this.value * b.value);
	}

	public Number divide(Number b) {
		return new Number (this.value / b.value);
	}

	public Number mod(Number b) {
		return new Number (this.value % b.value);
	}
}
