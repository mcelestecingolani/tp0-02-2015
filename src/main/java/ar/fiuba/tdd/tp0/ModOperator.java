package ar.fiuba.tdd.tp0;

public class ModOperator implements Operator {

	@Override
	public Number execute(Number a, Number b) {
		return a.mod(b);
	}

}
