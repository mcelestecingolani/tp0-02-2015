package ar.fiuba.tdd.tp0;

public class TimesOperator implements Operator {

	@Override
	public Number execute(Number a, Number b) {
		return a.times(b);
	}

}
