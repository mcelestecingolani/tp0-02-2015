package ar.fiuba.tdd.tp0;

public class BinaryOperator implements PolishAtom {
	
	Operator op;
	
	public BinaryOperator (Operator op){
		this.op = op;
	} 

	@Override
	public void operate(MyStack src, MyStack result) {
		Number right = (Number) result.pop();
		Number left = (Number) result.pop();
		PolishAtom res =op.execute(left, right);
		result.push(res);
	}

}

